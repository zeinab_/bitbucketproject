
<div>

  <!-- Button trigger modal -->
  <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalCenter" >
  ADD Category</button>


  <!-- table of show data  -->
  <table class="table table-striped">
    <thead>
     <tr>
       <th scope="col">#</th>
       <th scope="col">Category Name ar</th>
       <th scope="col">Category Name en</th>
       <th scope="col">img</th>
       <th scope="col">controls</th>

     </tr>
    </thead>

    <tbody>
       <tr>
         @foreach($categ as $info)
        <th scope="row">{{$info->id}}</th>
        <td>{{$info->name_ar}}</td>
        <td>{{$info->name_en}}</td>
        <td>
          <img src="{{asset('storage/category/'.$info->img)}}" width="50px" height="50px"></td>
        <td>
          <button id="cat_edit" wire:click="e_data({{$info->id}})" ><i class="fas fa-pencil-alt"  style=" margin-right: 25px;margin-left: 10px; color: black" data-toggle="modal" data-target="#editcategory"></i></button>

          <button  class="fas fa-trash-alt" style="color: black" data-toggle="modal" data-target="#exampleModalLabel"  wire:click="del({{$info->id}})" ></button>
        </td>
      </tr>
     @endforeach
   </tbody>
  </table>
  @include('livewire.del')
@include('livewire.edit')
@include('livewire.add')
  

<!--  -->
</div>



