<div>
<div wire:ignore.self class="modal fade" id="editcategory" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true" style="margin-top:200px">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">edit data</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
       <form enctype="multipart/form-data" wire:submit.prevent="edit_cat">
        <input type="hidden" name="idd" wire:model="idd">
       <div class="form-group">
    
       <label for="exampleInputEmail1">Category Name EN</label>
         <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter Product Name in English" wire:model="e_name_en">
    
      </div>
      <div class="form-group">
        <label for="exampleInputEmail1">Category Name Ar</label>
    <input type="text" class="form-control" id="exampleInputEmail1"  placeholder="Enter Product Name in Arabic" wire:model="e_name_ar" >
  </div>
  
  <div class="form-group">
    <input type="hidden" wire:model="old_imag" value="">
    <label >image</label>
    <input type="file" class="form-control" wire:model="e_img">
  </div>
  

        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary" >Submit</button>

      
</form>
      </div>
      
    </div>
  </div>
</div>    



</div>