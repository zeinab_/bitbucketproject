
<div>

<!-- Button trigger modal -->
<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalCenter" >
  ADD Product
</button>
<table class="table table-striped">
  <thead>
    <tr>
     <th>product_name ar</th>
     <th>product_name en</th>
     <th>price</th>
     <th>img</th>
     <th>category</th>
     <th>controls</th>
  </tr>
  </thead>
  <tbody>
    @foreach($pro_info as $info)
    <tr>
     <td>{{$info->name_ar}}</td>
     <td>{{$info->name_en}}</td>
     <td>{{$info->price}}</td>
     <td><img src="{{asset('storage/product/'.$info->img)}}" width="50px" height="50px"></td>
     <td>@php $x=DB::table('category')->where('id',$info->cat_id)->pluck('name_en');
     echo $x[0]; @endphp</td>
     <td>
       <button class="fas fa-pencil-alt" data-toggle="modal" data-target="#exampleModalCenter1" wire:click="pro_edit({{$info->id}})"></button>
       <button class="fas fa-trash-alt" data-toggle="modal" data-target="#exampleModalCenter2" wire:click="delete_pro({{$info->id}})"></button>
     </td>
    </tr>
    @endforeach
  </tbody>
</table>



<!-- Modal add -->
<div wire:ignore.self class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" style="margin-top: 50px">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">ADD Product</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form enctype="multipart/form-data" wire:submit.prevent="cat_add">
  <div class="form-group">
    <label for="exampleInputEmail1">Product Name</label>
    <input type="text" class="form-control" id="exampleInputEmail1"  placeholder="Enter Product Name in English" wire:model="p_name_en">
  </div>
  <div class="form-group">
    <label for="exampleInputEmail1">Product Name Ar</label>
    <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Enter Product Name in Arabic" wire:model="p_name_ar">
  </div>
  <div class="form-group">
    <label for="exampleInputPassword1">Price</label>
    <input type="text" class="form-control"  placeholder="Enter price in numbers" wire:model="p_price">
  </div>
  <div class="form-group">
    <label >image</label><br>
    <input type="file" wire:model="p_img">
  </div>
  <div class="form-group">
    <label for="e">Category</label>
    <select wire:model="p_cate" class="form-control">
      @foreach($cate_data as $data)
    <option value="{{$data->id}}">{{$data->name_en}}</option>
    @endforeach
    </select>
  </div>

  <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Submit</button>

      </div>
</form>
      </div>
      
    </div>
  </div>
</div>

<!-- model edit -->
<div wire:ignore.self class="modal fade" id="exampleModalCenter1" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" style="margin-top: 50px">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">EDIT Product</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

        <form enctype="multipart/form-data" wire:submit.prevent="pro_co_edit">
  <div class="form-group">
    <input type="hidden" wire:model="pe_id">
    <label for="exampleInputEmail1">Product Name</label>
    <input type="text" class="form-control" id="exampleInputEmail1"  placeholder="Enter Product Name in English" wire:model="pe_name_en">
  </div>
  <div class="form-group">
    <label for="exampleInputEmail1">Product Name Ar</label>
    <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Enter Product Name in Arabic" wire:model="pe_name_ar">
  </div>
  <div class="form-group">
    <label for="exampleInputPassword1">Price</label>
    <input type="text" class="form-control"  placeholder="Enter price in numbers" wire:model="pe_price">
  </div>
  <div>
  </div>
  <div class="form-group">
    <label >image</label><br>
    <input type="hidden" wire:model="pe_oldimg" >
    <input type="file" wire:model="pe_img">
  </div>
  <div class="form-group">
    <label for="e">Category</label>
    <select wire:model="pe_cate" class="form-control">
      @foreach($cate_data as $data)
    <option value="{{$data->id}}">{{$data->name_en}}</option>
    @endforeach
    </select>
  </div>

  <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Submit</button>

      </div>
</form>
      </div>
      
    </div>
  </div>
</div>


<div wire:ignore.self class="modal fade" id="exampleModalCenter2" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" style="margin-top: 50px">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Delete Product</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      Are you sure delete this product?
  </div>

  <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary" wire:click="del_conf">Submit</button>

      </div>
      </div>
      
    </div>
  </div>
</div>




</div>

