@include('admin.includes.header')

 
<button class="btn btn-primary" style="margin-bottom: 10px;background-color: black;border-color: black;font-size: 20px" data-toggle="modal" data-target=".bd-example-modal">add
</button>
<table class="table table-striped">
	<thead>
		<th>name</th>
		<th> email</th>
		<th>controls</th>
	</thead>
	<tbody>
		@foreach($data as $row)
		<tr>
			<td>{{$row->name}}</td>
			<td>{{$row->email}}</td>
			<td>
          <button class="fas fa-pencil-alt btn_edit"  style=" margin-right: 25px;
              margin-left: 10px;" data-target=".bd-example-mod" data-toggle="modal" id_e_u="{{$row->id}}" ></button>
        <button  class="fas fa-trash-alt edel"  vale_id="{{$row->id}}" data-toggle="modal" data-target="#exampleModal" lang="@php echo App::getLocale() @endphp" style="color: black"></button>
    </td>
		</tr>
		@endforeach
	</tbody>


 </table>


	<!-- Large modal add user-->
<div class="modal fade bd-example-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
    	<div class="modal-header">
    		<h4 id="myLargeModalLabel" class="modal-title">
        add</h4>
    		<button class="close" type="button" data-dismiss="modal" aria-label="Close"></button>
    		<span aria-hidden="true">x</span>
    	</div>
    	<div class="modal-body">
    		<form method="POST" class="aduser"  >
    			@csrf
              <div class="form-group">
               <label for="formGroupExampleInput">name</label>
               <input type="text" class="form-control name_u" id="formGroupExampleInput" placeholder="{{__('layout.Enter')}} {{__('layout.ProjectName_en')}}" name="name">
               
             
             </div>
              
             <div class="form-group">
             <label for="formGroupExampleInput2">email</label>
             <input type="email" class="form-control email_u" id="formGroupExampleInput2" placeholder="{{__('layout.Enter')}} {{__('layout.Client email')}}" name="email">
              
             </div>
              <div class="form-group">
             <label for="formGroupExampleInput2">password</label>
             <input type="password" name="pass" class="form-control pass_u" >
         </div>
             


         <div class="modal-footer">
        
        <button type="button" class="btn btn-primary addus">{{__('layout.submit')}}</button>
      </div>
            </form>

  </div>
    </div>
  </div>
</div>
<!-- model edit -->

<div class="modal fade bd-example-mod" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="exampleModa" style="margin-top:50px">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
    	<div class="modal-header">
    		<h4 id="myLargeModalLabel" class="modal-title">
        edit </h4>
    		<button class="close" type="button" data-dismiss="modal" aria-label="Close"></button>
    		<span aria-hidden="true">x</span>
    	</div>
    	<div class="modal-body">
    		<form method="POST" class="edituser"  >
    			@csrf
                @method('PUT') 
    			<input type="hidden" name="id_ee" class="id_eee">
              <div class="form-group">
               <label for="formGroupExampleInput">name</label>
               <input type="text" class="form-control name_u_e"  placeholder="" name="ename">
               
             
             </div>
              
             <div class="form-group">
             <label for="formGroupExampleInput2">email</label>
             <input type="email" class="form-control email_u_e" placeholder=" " name="eemail">
              
             </div>
              <div class="form-group">
             <label for="formGroupExampleInput2">password</label>
             <input type="password" name="epass" class="form-control pass_u_e" >
         </div>
             


         <div class="modal-footer">
        
        <button type="button" class="btn btn-primary edus">{{__('layout.submit')}}</button>
      </div>
            </form>

  </div>
    </div>
  </div>
</div>
<!-- Button trigger modal -->


<!-- Modaldel -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="margin-top: 50px">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">delete</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        do you want delete this user?
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary cedel">Save changes</button>
      </div>
    </div>
  </div>
</div>
@include('admin.includes.footer')