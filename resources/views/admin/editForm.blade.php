@include('admin.includes.header')
<form method="POST" action="{{url('en/admin/confEdit/'.$data->id)}}" enctype="multipart/form-data" >
    			@csrf
          @method('PUT')
            <input type="hidden" value="{{$data->id}}" name="id" id="id">

              <div class="form-group">
               <label for="formGroupExampleInput">ProjectName_en</label>
               <input type="text" class="form-control" id="formGroupExampleInput" value="{{$data->projName}}" name="projen">
               @error('projen')
             <small class="text text-danger" id="p_en">{{$message}}</small>
             @enderror
             </div>

             
             <div class="form-group">
             <label for="formGroupExampleInput2">ProjectName_ar</label>
             <input type="text" class="form-control" id="formGroupExampleInput2" value="{{$data->proj_ar}}" name="projar">
             @error('projar')

              <small class="text text-danger" id="p_ar">{{$message}}</small>
              @enderror
             </div>
             <div class="form-group">
             <label for="formGroupExampleInput2">cost</label>
             <input type="text" class="form-control" id="formGroupExampleInput2" value="{{$data->cost}}"name="cost">
              @error('cost')

              <small class="text text-danger" id="cost">{{$message}}</small>
              @enderror


             </div>
             <div class="form-group">
             <label for="formGroupExampleInput2">client Name</label>
             <input type="text" class="form-control" id="formGroupExampleInput2" value="{{$data->clientName}}" name="clientname">
             @error('clientname')
              <small class="text text-danger" id="c_n">{{$message}}</small>
              @enderror

             
             </div>
             <div class="form-group">
             <label for="formGroupExampleInput2">Description_en</label>
             <input type="text" class="form-control" id="formGroupExampleInput2" value="{{$data->description}}" name="desc_en">
             @error('desc_en')

              <small class="text text-danger" id="d_en">{{$message}}</small>
              @enderror
             
             </div>
             <div class="imgss">@foreach($image as $image)
      <img width="75px" height="75px" src="{{asset('photos/projects/'.$image)}}"><input type="checkbox" id="imgas" name="imgas[]" value="{{$image}}"><br><br>

              @endforeach

             </div>
             <div class="form-group ap_input">
             <label for="formGroupExampleInput2">images</label>
             <input type="file" class="form-control v" id="formGroupExampleInput2" name="img[]" multiple accept=".jpg ,.png,.gif,.jpeg"><i class="fas fa-plus add"  ></i>

             
             </div>
              <div class="form-group">
             <label for="formGroupExampleInput2">Description_ar</label>
             <input type="text" class="form-control" id="formGroupExampleInput2" value="{{$data->desc_ar}}" name="desc_ar">
             @error('desc_ar')
              <small class="text text-danger" id="d_ar">{{$message}}</small>
             @enderror
             </div>
             <div class="form-group">
             <label for="formGroupExampleInput2">Client email</label>
             <input type="email" class="form-control" id="formGroupExampleInput2" value="{{$data->emailClient}}"name="email">
             @error('email')
              <small class="text text-danger" id="email">{{$message}}</small>
              @enderror
             </div>
         <div class="modal-footer">
        
        <button type="submit" class="btn btn-primary " >submit</button>
      </div>
            </form>
            @include('admin.includes.footer')