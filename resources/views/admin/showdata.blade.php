@include('admin.includes.header')
@php echo session()->get('id');  @endphp
<button class="btn btn-primary ins_btn" style="margin-bottom: 10px;background-color: black;border-color: black;font-size: 20px" data-toggle="modal" data-target=".bd-example-modal-lg">
	<i class="fas fa-plus" style="margin-right: 5px;font-size:20px "></i>
  {{__('layout.insert')}}
</button>
  
<table class="table table-dark" style="width: 980px">
	<thead>
		<tr>
      @php if(App::getLocale()=="ar"){ @endphp
			<th>{{__('layout.ProjectName')}}</th>
      <th>{{__('layout.Cost')}}</th>
			<th>{{__('layout.ClientName')}}</th>
			<th>{{__('layout.Description')}}</th>
			<th>{{__('layout.EmailClient')}}</th>
			<th>{{__('layout.Controls')}}</th>
    @php }@endphp
    @php if(App::getLocale()=="en"){ @endphp
      <th>ProjectName</th>
      <th>Cost</th>
      <th>ClientName</th>
      <th>Description</th>
      <th>EmailClient</th>
      <th>Controls</th>
    @php }@endphp
		</tr>
	</thead>
  @php if(App::getLocale()=="ar"){ @endphp
  @foreach($data as $k=> $row)
	<tr class="tr{{$row->id}}">
		    <!-- <td>{{$row->projName}}</td> -->
  	<td>{{$row->proj_ar}}</td>
	
 	      <td>{{$row->cost}}</td>
		    <td>{{$row->clientName}}</td>
<!-- 		    <td>{{$row->description}}</td>
 -->
 		    <td>{{$row->desc_ar}}</td>
       <td>{{$row->emailClient}}</td>
        
<td>
        <a id="a_edit" href= " ">
          <i class="fas fa-pencil-alt"  style=" margin-right: 25px;
              margin-left: 10px;"></i></a>
            @role('admin')  
        <button  class="fas fa-trash-alt del" proj="{{$row->proj_ar}}" val_id="{{$row->id}}" data-toggle="modal" data-target="#exampleModa" lang="@php echo App::getLocale() @endphp"></button>
       @endrole

		</td>
	</tr>
    @endforeach
@php } @endphp


@php if(App::getLocale()=="en"){ @endphp
  @foreach($data as $k=> $row)
  <tr class="tr{{$row->id}}">
        <td>{{$row->projName}}</td>
    <!-- <td>{{$row->proj_ar}}</td> -->
  
        <td>{{$row->cost}}</td>
        <td>{{$row->clientName}}</td>
       <td>{{$row->description}}</td>

        <!-- <td>{{$row->desc_ar}}</td> -->
       <td>{{$row->emailClient}}</td>
        
<td>
                   <a id="a_edit" href= 
                   "{{url('en/admin/edit/'.$row->id)}}">

               <i class="fas fa-pencil-alt"  style=" margin-right: 25px;
              margin-left: 10px;"></i></button>
           @role('admin')    
        <button  class="fas fa-trash-alt del" proj="{{$row->projName}}" val_id="{{$row->id}}" data-toggle="modal" data-target="#exampleModa" lang="@php echo App::getLocale() @endphp" ></button>
      @endrole

    </td>
  </tr>
    @endforeach
@php } @endphp
</table>
</div>
 
</div>

<!-- Large modal add project-->
<div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
    	<div class="modal-header">
    		<h4 id="myLargeModalLabel" class="modal-title">
        {{__('layout.ADD project')}}</h4>
    		<button class="close" type="button" data-dismiss="modal" aria-label="Close"></button>
    		<span aria-hidden="true">x</span>
    	</div>
    	<div class="modal-body">
    		<form method="POST"  enctype="multipart/form-data" id="form">
    			@csrf
              <div class="form-group">
               <label for="formGroupExampleInput">{{__('layout.ProjectName_en')}}</label>
               <input type="text" class="form-control" id="formGroupExampleInput" placeholder="{{__('layout.Enter')}} {{__('layout.ProjectName_en')}}" name="projen">
               
             <small class="text text-danger" id="p_en"></small>
             </div>

             <div class="form-group">
             <label for="formGroupExampleInput2">{{__('layout.ProjectName_ar')}}</label>
             <input type="text" class="form-control" id="formGroupExampleInput2" placeholder=" {{__('layout.Enter')}}{{__('layout.ProjectName_ar')}}" name="projar">
              <small class="text text-danger" id="p_ar"></small>
             </div>
             <div class="form-group">
             <label for="formGroupExampleInput2">{{__('layout.Cost')}}</label>
             <input type="text" class="form-control" id="formGroupExampleInput2" placeholder="{{__('layout.Enter')}}{{__('layout.Cost')}}"name="cost">
              <small class="text text-danger" id="cost"></small>

             </div>
             <div class="form-group">
             <label for="formGroupExampleInput2">{{('layout.client Name')}}</label>
             <input type="text" class="form-control" id="formGroupExampleInput2" placeholder="{{__('layout.Enter')}}{{('layout.client Name')}}" name="clientname">
              <small class="text text-danger" id="c_n"></small>

             
             </div>
             <div class="form-group">
             <label for="formGroupExampleInput2">{{__('layout.Description_en')}}</label>
             <input type="text" class="form-control" id="formGroupExampleInput2" placeholder="{{__('layout.Enter')}} {{__('layout.Description_en')}}" name="desc_en">
              <small class="text text-danger" id="d_en"></small>
             
             </div>
             <div class="form-group ap_input">
             <label for="formGroupExampleInput2">{{__('layout.images')}}</label>
             <input type="file" class="form-control v" id="formGroupExampleInput2" placeholder="Enter Description of project" name="img[]" multiple accept=".jpg ,.png,.gif,.jpeg" >
             <i class="fas fa-plus add"  ></i>

             
              <small class="text text-danger" id="img"></small>
             </div>
              <div class="form-group">
             <label for="formGroupExampleInput2">{{__('layout.Description_ar')}}</label>
             <input type="text" class="form-control" id="formGroupExampleInput2" placeholder="{{__('layout.Enter')}} {{__('layout.Description_ar')}}" name="desc_ar">
              <small class="text text-danger" id="d_ar"></small>
             
             </div>
             <div class="form-group">
             <label for="formGroupExampleInput2">{{__('layout.Client email')}}</label>
             <input type="email" class="form-control" id="formGroupExampleInput2" placeholder="{{__('layout.Enter')}} {{__('layout.Client email')}} "name="email">
              <small class="text text-danger" id="email"></small>
             </div>
         <div class="modal-footer">
        
        <button type="button" class="btn btn-primary addpro">{{__('layout.submit')}}</button>
      </div>
            </form>

  </div>
    </div>
  </div>
</div>

<!-- delete modal -->


<div class="modal fade m_del" id="exampleModa" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">
          {{__('layout.Delete project')}}</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <!-- <span aria-hidden="true" style="text-align: left">&times;</span> -->
        </button>
      </div>
      <div class="modal-body contant">
        <!-- {{__('layout.msgdel')}} -->
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('layout.Close')}}</button>
        <form method="POST" class="f_del">
                @csrf
                @method('DELETE')
        <button type="submit" class="btn btn-primary conf_del">{{__('layout.Confirm')}}</button>
      </form>
      </div>
    </div>
  </div>
</div>
<!-- edit data -->
<div class="modal fade bd-example-modal-lgg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 id="myLargeModalLabel" class="modal-title">
        EDIT project</h4>
        <button class="close" type="button" data-dismiss="modal" aria-label="Close"></button>
        <span aria-hidden="true">x</span>
      </div>
      <div class="modal-body">
<form method="POST"  enctype="multipart/form-data" >
          @csrf
          @method('PUT')
  <input type="hidden" value="" name="id" id="id">

              <div class="form-group">
               <label for="formGroupExampleInput">ProjectName_en</label>
               <input type="text" class="form-control cz" id="projen" value="" name="projen">
               @error('projen')
             <small class="text text-danger" id="p_en">{{$message}}</small>
             @enderror
             </div>

             
             <div class="form-group">
             <label for="formGroupExampleInput2">ProjectName_ar</label>
             <input type="text" class="form-control" id="projar" value="" name="projar">
             @error('projar')

              <small class="text text-danger" id="p_ar">{{$message}}</small>
              @enderror
             </div>
             <div class="form-group">
             <label for="formGroupExampleInput2">cost</label>
             <input type="text" class="form-control" id="co" value=""name="cost">
              @error('cost')

              <small class="text text-danger" id="cost">{{$message}}</small>
              @enderror


             </div>
             <div class="form-group">
             <label for="formGroupExampleInput2">client Name</label>
             <input type="text" class="form-control" id="client_name" value="" name="clientname">
             @error('clientname')
              <small class="text text-danger" id="c_n">{{$message}}</small>
              @enderror

             
             </div>
             <div class="form-group">
             <label for="formGroupExampleInput2">Description_en</label>
             <input type="text" class="form-control" id="des_en" value="" name="desc_en">
             @error('desc_en')

              <small class="text text-danger" id="d_en">{{$message}}</small>
              @enderror
             
             </div>
             <div class="imgss">
               
               


             </div>
             
             <div class="form-group ap_input">
             <label for="formGroupExampleInput2">images</label>
             <input type="file" class="form-control v" id="formGroupExampleInput2" name="img[]" multiple accept=".jpg ,.png,.gif,.jpeg"><i class="fas fa-plus add"  ></i>

             
             </div>
              <div class="form-group">
             <label for="formGroupExampleInput2">Description_ar</label>
             <input type="text" class="form-control" 
             id="ard" value="" name="desc_ar">
             @error('desc_ar')
              <small class="text text-danger" id="d_ar">{{$message}}</small>
             @enderror
             </div>
             <div class="form-group">
             <label for="formGroupExampleInput2">Client email</label>
             <input type="email" class="form-control" id="client_email" value=""name="email">
             @error('email')
              <small class="text text-danger" id="email">{{$message}}</small>
              @enderror
             </div>
         <div class="modal-footer">
        
        <button type="submit" class="btn btn-primary " >submit</button>
      </div>
            </form>
            </div>
    </div>
  </div>
</div>
@include('admin.includes.footer')