
  </div>

    </div>
    </div>

    </div></div>

</script>
<!-- Jquery JS-->
    <script src="{{asset('admin/vendor/jquery-3.2.1.min.js')}}"></script>
    <!-- Bootstrap JS-->
    <script src="{{asset('admin/vendor/bootstrap-4.1/popper.min.js')}}"></script>

    <script src="{{asset('admin/vendor/bootstrap-4.1/bootstrap.min.js')}}"></script>
    <!-- Vendor JS       -->
    <script src="{{asset('admin/vendor/slick/slick.min.js')}}">
    </script>
    <script src="{{asset('admin/vendor/wow/wow.min.js')}}"></script>
    <script src="{{asset('admin/vendor/animsition/animsition.min.js')}}"></script>
    <script src="{{asset('admin/vendor/bootstrap-progressbar/bootstrap-progressbar.min.js')}}">
    </script>
    <script src="{{asset('admin/vendor/counter-up/jquery.waypoints.min.js')}}"></script>
    <script src="{{asset('admin/vendor/counter-up/jquery.counterup.min.js')}}">
    </script>
    <script src="{{asset('admin/vendor/circle-progress/circle-progress.min.js')}}"></script>
    <script src="{{asset('admin/vendor/perfect-scrollbar/perfect-scrollbar.js')}}"></script>
    <script src="{{asset('admin/vendor/chartjs/Chart.bundle.min.js')}}"></script>
    <script src="{{asset('admin/vendor/select2/select2.min.js')}}">
    </script>

    <!-- Main JS-->
    <script src="{{asset('admin/js/main.js')}}"></script>
    <!-- design page -->

     <script type="text/javascript">
         $(document).ready(function() {

            
            $('.addpro').click(function(event) {
                event.preventDefault();
                var formdata=new FormData($('#form')[0]);
                $.ajax({
                    url: "{{route('addProject',app::getLocale())}}",
                    type: 'POST',
                    data: formdata,
                    processData:false,
                    contentType:false,
                })
                .done(function(data) {
                    console.log(data);
                    console.log("su");

                    if (data.projen)
                     {
                        $('#p_en').html(data.projen);
    
                     }
                     if (data.projar)
                     {
                        $('#p_ar').html(data.projar);
    
                     }
                     if (data.cost)
                     {
                        $('#cost').html(data.cost);
    
                     }
                     if (data.clientname)
                     {
                        $('#c_n').html(data.clientname);
    
                     }
                     if (data.desc_en)
                     {
                        $('#d_en').html(data.desc_en);
    
                     }
                     if (data.desc_ar)
                     {
                        $('#d_ar').html(data.desc_ar);
    
                     }
                     if (data.email)
                     {
                        $('#email').html(data.email);
    
                     }
                     if (data.img)
                     {
                        $('#img').html(data.img);
    
                     }
                     })

                .fail(function(err)
                {
                     console.log(err);

                });
            });
            var id;
            $(".del").click(function(t) {
                t.preventDefault();
                id= $(this).attr('val_id');
                var proj_name=$(this).attr("proj");
                var lang=$(this).attr("lang");
                if(lang=="en")
                {
                   $(".contant").html("Do yo want delete project "+proj_name);             
                      }
                if(lang=="ar")
                { 
                $(".contant").html("هل انت متأكد من حذف "+proj_name);
 
                }
            });
            $(".conf_del").click(function(eve) {
                eve.preventDefault();
               // console.log(id);
                $.ajax({
                    url: "{{route('dele',app::getLocale())}}",
                    type: 'GET',
                    dataType: 'json',
                    data: {"_token": "{{ csrf_token() }}",id: id},
                })
                .done(function(data) {
                    console.log(data);
                    $('.m_del').modal('hide');
                    $(".tr"+id).remove();
                })
                .fail(function(er) {
                    console.log(er);
                
                
                
            });

         });
            // admin insert

            $('.addA').click(function(add) {
                 var formdat=new FormData($('.form')[0]);

                add.preventDefault();
               $.ajax({
                   url: '{{route("admin.add",app::getLocale())}}',
                   type: 'POST',
                   data: formdat,
                    processData:false,
                    contentType:false,
                    cache:false,
               })
               .done(function(data) {
                   console.log(data);
                   $('.show').hide();
               })
               .fail(function(r) {
                   console.log(r);
               })
               
            });
            // admin update



            // admin delete
            var Admin_id;
            $('.delA').click(function(eent) {
                eent.preventDefault();
               Admin_id= $(this).attr('Admin_id');
                var Admin_name=$(this).attr('Uname');

                  $('.contentA').html("Do you want delete"+Admin_name);
                



            });

            $('.conf_delA').click(function(eA)
             {
                eA.preventDefault();
                $.ajax({
                    url: '{{route('delAdmin',app::getLocale())}}',
                    type: 'GET',
                    data: {id: Admin_id},
                })
                .done(function(data) {
                     $('.A_del').modal('hide');
                    $(".row"+Admin_id).remove();
                })
                .fail(function(e) {
                    console.log(e);
                })
                
                

            });
            var c_id;
            $('.edit').click(function(edit) {
                edit.preventDefault();
               c_id=$(this).attr('edit_id');
                console.log(c_id);
                $('.hide_id').attr('value',  c_id);
                $.ajax({
                    url: '{{route("edit.show",app::getLocale())}}',
                    type: 'POST',
                    dataType: 'json',
                    data: {"_token": "{{ csrf_token() }}",id: c_id},
                })
                .done(function(data) {
                    console.log(data);
                     $('.ename').val(data.username);
                     $('.eemail').val(data.email);
                     // $('.epriv').val(data.priv);
                     // $('.imgss').append('<img src="'+imgs+'" />')
                     // $('.s_img').attr("src",
                     //    "{{asset('photos/admin/"+data.img+"')}}");

                      if(data.priv==0)
                      {
                        $(".supper").attr("selected","true");
                        $(".employee").removeAttr('selected');


                      }
                      else if(data.priv==1)
                      {
                         $(".supper").removeAttr("selected");
                         $(".employee").attr("selected","true");

                      }
                })
                .fail(function(orr) {
                    console.log(orr);
                }) 

            });
            $(".c_edit").click(function(edit) {
                edit.preventDefault();
                var formdat_update=new FormData($('.edit_form')[0]);

                console.log(c_id);
               $.ajax({
                    url: '{{route("c.edit",app::getLocale())}}',
                    type: 'POST',
                    processData:false,
                    contentType:false,
                    cache:false,

                    data:formdat_update ,
                })
                .done(function(x) {
                    console.log(x);
                })
                .fail(function(r) {
                    console.log(r);
                })
                
                

            });
            var i=1;
        $('.add').click(function(event) {
            event.preventDefault();
         
            
            console.log("1");
             $('.ap_input').append('<input type="file" class="form-control img'+i+'" id="exampleInputphone" style="width:430px "  name="img[]"><button class="btn mi" id="img'+i+'" ><i class="fas fa-minus-circle "></i></button> ');
             i++;
            

        });
        $(document).on('click','.mi',function(ev){
            ev.preventDefault();
             var va=$(this).attr('id');
             
              $(this).prev().remove();
              $(this).remove();


        });
        $('.addus').click(function(ve) {
                ve.preventDefault();
                var name= $('.name_u').val();
                var email=$('.email_u').val();
                var password=$('.pass_u').val();
            
            // var formda=new FormData($('.aduser')[0]);
            // console.log(formda);
               $.ajax({
                   url: '{{route("adduser",app::getLocale())}}',
                   type: 'POST',
                   data: {"_token": "{{ csrf_token() }}",
                           name:name,email:email
                           ,password:password},
                    
               })
               .done(function(data) {
                   // console.log(data);
                   $('.show').hide();
               })
               .fail(function(r) {
                   console.log(r);
               })

         });
        var ide;
        $('.btn_edit').click(function(edit) {
            edit.preventDefault();
            ide=$(this).attr('id_e_u');

            $.ajax({
                url: '{{route("show",app::getLocale())}}',
                type: 'POST',
                dataType: 'json',
                data: {"_token": "{{ csrf_token() }}",id:ide},
            })
            .done(function(da_e) {
                // console.log(da_e);
                $('.id_eee').val(da_e.id);
                $('.name_u_e').val(da_e.name);
                $('.email_u_e').val(da_e.email);


                
            })
            .fail(function(x) {
                console.log(x);
            })
            
            

          });
        $('.edus').click(function(up) {
          up.preventDefault();
           var xname= $('.name_u_e').val();
            var xemail=$('.email_u_e').val();
            var xpass=$('.pass_u_e').val();
            console.log(xpass);
            $.ajax({
                url: '{{route("updates",app::getLocale())}}',
                type:'POST',
                data:{"_token": "{{ csrf_token() }}",id:ide,name:xname,email:xemail,pass:xpass,_method:"PUT"},
            })
            .done(function(d) {
                console.log(d);
                console.log("d");

            })
            .fail(function(c) {
                console.log(c);
                console.log("c");

            })
            
            

        });
        var exid;
        $('.edel').click(function(z) {
            z.preventDefault();
            exid=$(this).attr('vale_id');

        });
        $('.cedel').click(function() {
            $.ajax({
                url: '{{route("destory",app::getLocale())}}',
                type: 'GET',
                 data: {id:exid},
            })
            .done(function(sx) {
                console.log(sx);
                console.log('c');
            })
            .fail(function(or) {
                console.log(or);
            })
            
            
        });


        

    });


     </script>

</body>

</html>
