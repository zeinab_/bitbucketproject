<?php

return
 [ 'Dashboard'=>'الرئيسيه',
  'Charts'=>'خرائط',
  'Tables'=>'جداول',
  'Forms'=>'استماره',
  'Calender'=>'التقويم',
  'Maps'=>'خرائط',
  'Pages'=>'صفحات',
  'Laravel'=>'لارافل',
  'insert'=>'اضافه',
  'UI Elements'=>'العناصر',
  'Search for datas'=>'البحث عن بيانات',
  'reports'=>'التقارير',
  'Account'=>'الحساب',
  'Setting'=>'الأعدادات',
  'Billing'=>'الفاتوره',
  'Logout'=>'تسجيل الخروج',
  'ADD project'=>'اضافه مشروع',
  'ProjectName_en'=>'اسم المشروع باللغه الانجليزيه',
  'ProjectName_ar'=>'اسم المشروع باللغه العربيه',
  'client Name'=>'اسم العميل',
  'Cost'=>'التكلفه',
  'Description_en'=>'التفاصيل باللغه الانجليزيه',
  'images'=>'الصور',
  'Description_ar'=>'التفاصيل باللغه العربيه',
  'Client email'=>'البريد الالكترونى للعميل ',
  'submit'=>'ارسال',
  'Enter'=>'ادخل ',
  'client Name'=>'اسم العميل',
  'Delete project'=>'حذف مشروع',
  'Confirm'=>'تأكيد',
  'Close'=>'إغلاق',
  'ProjectName'=>'اسم المشروع',
  'ClientName'=>'اسم العميل',
  'Description'=>'التفاصيل ',
  'EmailClient'=>'البريد الالكترونى للعميل ',
  'Controls'=>'ادوات التحكم',
  'Add Admin'=>'إضافه أدمن',
  'Name'=>'الأسم',
  'Email'=>'البريد الألكترونى',
  'Priv'=>"الصلاحيه",
  'Image'=>'صوره'

]









?>