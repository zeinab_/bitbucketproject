<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Models\Categor;
use App\Models\Productss;

use App\Http\Resources\CategorResource;
use App\Http\Resources\CategorCollection;
use App\Http\Resources\ProductResource;
use App\Http\Resources\ProductCollection;



/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::get('/ca/{id}', function ($id) {
    return new CategorResource(Categor::findOrFail($id));
});
Route::get('/categ',function(){
return CategorResource::collection(Categor::all());
});
Route::get('/names', function () {
    return new CategorCollection(Categor::all());
});
Route::get('/pro', function () {
    return ProductResource::collection(Productss::all());
    // ->additional(['meta' => [
    //                 'key' => 'productinfo']]);
});



