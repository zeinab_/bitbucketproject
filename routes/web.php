<?php

use Illuminate\Support\Facades\Route;
use App\Http\Livewire\AddProduct;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::group(['prefix' => '{lang}'], function () {
    
  Route::prefix('admin')->group(function(){



 

 	// login
 	Route::get('/login','Admin\Auth\LoginController@showLoginForm'                                   )->name('admin.login');

	Route::post('/login','Admin\Auth\LoginController@login')->name("admin.submit");
	// logout
	 Route::post('/logout','Admin\Auth\LoginController@logout')->name('admin.logout');
   // first page in admin site table of designs(add,delete,updat) 
	 Route::get('/table',  'AdminController@getData')->name('admin.design');

    Route::post('/add' ,  'AdminController@add')->name('addProject');

     Route::get('/edit/{id}', "AdminController@editPage")->name('editpro');
    

     Route::put('/confEdit/{id}',  'AdminController@editOk');

     Route::get('/delete'       ,'AdminController@deleteData')->name("dele");
    // second page admin page(add ,delete,update)
	
	 Route::get('/data','Admin\AdminUserController@adminInfo');
	 Route::post('/addAdmin',"Admin\AdminUserController@addAdmin")->name('admin.add');
	 Route::post('/editshow','Admin\AdminUserController@updateAdmin')->name('edit.show');
	 Route::put('/c-edit','Admin\AdminUserController@updateA')->name('c.edit');

	 Route::get('/del','Admin\AdminUserController@adminDel')->name('delAdmin');
	 Route::get('/pre','AdminController@premission');
	  Route::get('/cate',function(){
	 	return view('cate');
	 });
	  Route::get('/product',function(){
	 	return view('productshow');
	 });
	  Route::get('/userpage','Admin\upController@userpage');
	  Route::post('/adduser','Admin\upController@create')->name('adduser');
	  Route::post('show','Admin\upController@edit')->name('show');
	  Route::put('updatee','Admin\upController@updat')->name('updates');
	  Route::get('des','Admin\upController@del')->name('destory');



  });
  });
// });

	Auth::routes();




	// user page
	Route::prefix('p_user')->group(function(){
		Route::get('/', function () {
		 return view('front.index');
      })->name('index');
     
      Route::get('User_login',function(){
	   return view('front.logiin');
       })->name('User_login');
      Route::get('regist_user',function(){
      	return view('front.sign');
      });
      Route::get('product/{id}',function($id){
      	return view('front.product',compact('id'));

      });
      Route::get('prodCart','User\cartController@addCart')->name('cart');
      Route::get('cartp',function(){
      	return view('front.cart');

      })->name('cartpage');
      Route::delete('remove','User\cartController@removeCart')->name('remo');
      Route::get('checkout','User\cartController@checkOut')->name('check');
      Route::get('continu','User\cartController@continu_check')->name('continu');

});