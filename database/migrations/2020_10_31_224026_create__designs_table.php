<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDesignsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('_designs', function (Blueprint $table) {
            $table->id();
            $table->string('projName')->unique();
            $table->string('proj_ar')->unique();
            $table->float('cost');
            $table->string('clientName');
            $table->string('description');
            $table->string('desc_ar');
            $table->string('emailClient');
            // $table->string('img');


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('_designs');
    }
}
