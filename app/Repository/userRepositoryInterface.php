<?php 
namespace App\Repository;

use Illuminate\DataBase\Eloquent\Model;
use Illuminate\DataBase\Eloquent\Collection;

interface userRepositoryInterface
{
	public function create(array $data);
	public function all();
	public function find($id);
	public function update(array $data,$id);
	public function delete($id);








}
?>