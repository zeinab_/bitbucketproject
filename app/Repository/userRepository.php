<?php 
namespace App\Repository;
use Illuminate\DataBase\Eloquent\Model;
use Illuminate\DataBase\Eloquent\Collection;
use App\User;
use Illuminate\Support\Facades\Hash;


class userRepository implements userRepositoryInterface
{
	protected $model;
	public function __construct(Model $model)
	{
		return $this->model =$model;
	}
	public function create(array $data)
	{
		$data['password']=Hash::make($data['password']);
		 return $this->model->create($data);
	}
	public function all(){
		return $this->model->all();
	}


	public function find($id){

		return $this->model->find($id);
	}
	public function update(array $data,$id)
    { 
    	  // return print_r($data);
        $record = $this->model->find($data['id']);
        return $record->update($data);
    }
    public function delete($id){
    	return $this->model->destroy($id);
    }




}

?>