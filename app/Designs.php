<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Designs extends Model
{
    protected $table="_designs";
    protected $fillable=["projName","proj_ar","cost",'clientName','description','desc_ar','emailClient','img'];
}
