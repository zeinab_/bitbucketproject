<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Productss extends Model
{
    protected $table="product";
    protected $fillable=["name_en","name_ar","img","price","buys","views","cat_id"];


    public function category()
    {
    	return $this->belongsTo('App\Models\Categor','cat_id');
    }
}

