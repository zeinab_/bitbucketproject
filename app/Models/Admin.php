<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;


class Admin extends Authenticatable

{     
	use HasRoles;
	protected $guard ="admin";
    protected $table="admins";
    protected $fillable=["username","email","password","priv"];
}
