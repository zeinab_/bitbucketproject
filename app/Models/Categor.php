<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Categor extends Model
{
    protected $table="category";
    protected $fillable=["name_ar","name_en","img"];
    
}
