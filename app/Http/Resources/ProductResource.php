<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\CatgorResource;
use App\Http\Resources\CatgorCollection;

class ProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

         return [
            "id"=>$this->id,
            "name_ar"=>$this->name_ar,
            "name_en"=>$this->name_en,
            "img"=>$this->img,
// 'category' => CategorResource::collection($this->whenLoaded('category')),
            'category' =>$this->category,
            "price"=>$this->price,

            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        
        ];
        // return parent::toArray($request);
    }
}
