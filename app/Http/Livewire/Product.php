<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Categor;
use App\Models\Productss;
use Livewire\WithFileUploads;
class Product extends Component
{ 
	public 
	$p_name_en,
	$p_name_ar,
	$p_price,
	$p_img,
	$p_cate,
	$pe_name_en,
	$pe_name_ar,
	$pe_price,
	$pe_img,
	$pe_cate,
	$pe_id,
	$pe_oldimg,
	$prod_id;
	use WithFileUploads;

    public function render()
    {
    	$cate_data   =Categor::get();
    	$product_data=Productss::get();
    	
        return view('livewire.product',['cate_data'=>$cate_data,'pro_info'=>$product_data]);
    }
    public function cat_add(){
    	 $validatedData = $this->validate([
            'p_name_ar' => 'required',
            'p_name_en' => 'required',
            'p_img' => 'required|image|mimes:jpeg,png,svg,jpg,gif|max:1024',
            'p_price' =>'required',
        ]);



       $imageName = $this->p_img->store('product','public');

       $imgn= str_replace("product/","","$imageName");


    	Productss::create([
    		'name_ar'=>$this->p_name_ar,
    		'name_en'=>$this->p_name_en,
    		'img'    =>$imgn,
    		'price'  =>$this->p_price,
    		'cat_id' =>$this->p_cate,
    		


    	]);

    	
    }
    public function pro_edit($id)
    {
    	$pro=Productss::find($id);
    	$this->pe_id=$id;
    	$this->pe_name_en=$pro->name_en;
    	$this->pe_name_ar=$pro->name_ar;
    	$this->pe_price=$pro->price ; 
    	$this->pe_oldimg=$pro->img ;
    	$this->pe_cate =$pro->cat_id;
    }
    public function pro_co_edit()
    {
    	$pro=Productss::find($this->pe_id);
    	if($this->pe_img)
    	{
    		unlink(public_path('storage/product/'.$this->pe_oldimg));

            $imageName = $this->pe_img->store('product','public');
            $imgn= str_replace("product/","","$imageName");
            $pro->update
            ([
                "img"=>$imgn
            ]);
    	}
    	$pro->update
    	([
    		'name_ar'=>$this->pe_name_ar,
    		'name_en'=>$this->pe_name_en,
    		'price'  =>$this->pe_price,
    		'cat_id' =>$this->pe_cate
    	]);
    }

    public function delete_pro($pro_id)
    {
    	$this->prod_id=$pro_id;

    }
    public function del_conf()
    {
    	$del=Productss::find($this->prod_id);
    	unlink(public_path('storage/product/'.$del->img));
    	$del->delete();
    }
}
