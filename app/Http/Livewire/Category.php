<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Categor;
use Livewire\WithFileUploads;
use Illuminate\Support\Facades\Storage;
class Category extends Component
{ 
    use WithFileUploads;
    public $e_name_en,$e_name_ar, $img,
           $name_ar,$name_en,$id_del,$idd,
           $e_img,$old_imag;
    public function render()
    {
       $cate_info= Categor::get();
        return view('livewire.category',['categ'=>$cate_info]);
    }
     function insert(){
        
        $validatedData = $this->validate([
            'name_ar' => 'required',
             'name_en' => 'required',

            'img' => 'required|image|mimes:jpeg,png,svg,jpg,gif|max:1024',
        ]);

        $imageName = $this->img->store('category','public');
     //            // $validatedData['name'] = $imageName;
     //    // dd($imageName);


       $imgn= str_replace("category/","","$imageName");


    	Categor::create([
            "name_ar"=>$this->name_ar,
            "name_en"=>$this->name_en,
            "img"=>$imgn
        ]);
    }
    public function e_data($id){

        $edit_info=Categor::find($id);
        $this->idd      =$id;
        $this->e_name_ar=$edit_info->name_ar;
        $this->e_name_en=$edit_info->name_en;
        $this->old_imag  =$edit_info->img;
        
    }
   public function edit_cat(){
    

        $edit_info=Categor::find($this->idd);
       
        
        if($this->e_img)
        {
          unlink(public_path('storage/category/'.$this->old_imag));

            $imageName = $this->e_img->store('category','public');
            var_dump($imageName);
            $imgn= str_replace("category/","","$imageName");
            $edit_info->update([
                "img"=>$imgn

            ]);


        }
        $edit_info->update([
            'name_ar'=>$this->e_name_ar,
            'name_en'=>$this->e_name_en
        ]);


    }

    
    public function del($id){
        $this->id_del=$id;

    }
    public function delete()
    {
        $del_cat=Categor::find($this->id_del);
       unlink(public_path('storage/category/'.$del_cat->img)); 
        $del_cat->delete();
    }

}
