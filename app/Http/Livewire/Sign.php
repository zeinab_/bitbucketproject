<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\User;
use Illuminate\Support\Facades\Hash;

class Sign extends Component
{
	public $pass_u,$email_u,$name_u;

    public function render()
    {
        return view('livewire.sign');
    }
    public function add_u()
    {
    	
    	User::create([
    		'name'=>$this->name_u,
    		'email'=>$this->email_u,
    		'password'=>Hash::make($this->pass_u)
    	]);
    	return redirect()->route('User_login');

    }
}
