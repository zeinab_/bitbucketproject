<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\User;
use DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;


class Login extends Component
{ public $lo_pass,$lo_email;
    public function render()
    {
        return view('livewire.login');
    }
    public function search(){
    	
    	if (Auth::guard('web')->attempt(['email'=>$this->lo_email,
            'password'=>$this->lo_pass])) 
        { 
            $id= DB::table('users')->where('email',$this->lo_email)->pluck('id');
             $session_user=session()->put('user',$id);
            // return print_r(session()->get('user'));

        	 return redirect()->intended(url('p_user/'));

          }
          return 'try again';
          

    }

}
