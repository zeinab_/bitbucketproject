<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Designs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use Illuminate\Support\Arr;
use DB;
use App\Models\Image;
use Illuminate\Support\Facades\Auth;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

// use auth;
class AdminController extends Controller

{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

     
    function add(Request $val)
    {          
        $rule=$this->getrules();
        $msg=$this->getmessage();
        $vali=Validator::make($val->all(),$rule,$msg);
         if($vali->fails())
         {
            
            return $vali->errors();
         }


    	
    	$res=Designs::create
    	([
    		'projName'    =>$val->projen,
    		'proj_ar'     =>$val->projar,
    		'cost'        =>$val->cost,
    		'clientName'  =>$val->clientname,
    		'description' =>$val->desc_en,
    		'desc_ar'     =>$val->desc_ar,
    		'emailClient' =>$val->email,
    	]);
        $img=$val->file('img');
        
         echo count($img);
        for ($i=0; $i <count($img) ; $i++)
         { 
    
            $file_ex=$img[$i]->getClientOriginalExtension();
               echo $file_ex;


        $img_name=md5(uniqid()).".".$file_ex;
        echo $img_name;

        $path=public_path("/photos/projects");

        $img_place=$img[$i]->move($path,$img_name);
         $lastid=$res->id;
         echo $lastid;
         
        Image::create([
            'proj_id' =>$lastid,
            'image'=>$img_name,
        ]);

}
        // return "done";

    }

    protected function getmessage(){
       return $msg=[
            // 'projen.ltr'      =>"please enter words in english",
            'projen.required' =>"this filed is required",
            'projen.unique'   =>"this project already exist",
            // 'projar.string_arabic'   =>"please enter words in arabic",
            'projar.required' =>"this filed is required",
            'projar.unique  ' =>"this project already exist",
            'cost.required'   =>"this filed is required",
            'cost.numeric'    =>"enter numbers only",
            'desc_en.required'=>"this filed is required",
            'desc_ar.required'=>"this filed is required",
            'email.required'  =>"this filed is required",
            'email.email'     =>"this email not vaild",
            // 'img.required'    =>"this filed is required",
            'clientname.required'=>"this filed is required"
            // 'img.mimes'       =>"image not vaild "
            
        ];

    }
    protected function getrules()
    {
        return $rules=[
            'projen'    =>'required|unique:_designs,projName',
            'projar'    =>'required|unique:_designs,proj_ar',
            'cost'      =>'required|numeric',
            'clientname'=>'required|string',
            'desc_en'   =>'required',
            'desc_ar'   =>'required',
            'email'     =>'required|email',
            // 'img'       =>'required'
        ];

    }
    function getData()
    { 
        $image=array();
        $data= Designs::get();
          
        return view('admin/showdata',['data'=>$data]);


    }
    function editPage(Request $req)

    {  
        
        $data= Designs::find($req->id);
        $image = DB::table('images')->where('proj_id', $req->id)->pluck('image');
        
        
     return view('admin.editform',["data"=>$data,"image"=>$image]);

    }

    function editOk(Request $req,$id)
    {
        echo $req->id;

       $data= Designs::find($req->id);
       $image = DB::table('images')->where('proj_id', $req->id)->pluck('image');
       if(!empty($req->input('imgas')))
       {
         $x= count($req->input('imgas'));
         // print_r($req->input('imgas'));

         for ($i=0; $i <$x ; $i++) 
         { 

           $new_i=$req->input('imgas')[$i];
           $path="/photos/projects/".$new_i;
           unlink(public_path($path));
           DB::table('images')->where('image',$new_i)->delete();
         } 
        }
       if($req->hasfile('img'))
       {
         $img=$req->file('img');
        
         echo count($img);
         for ($i=0; $i <count($img) ; $i++)
          { 
    
            $file_ex=$img[$i]->getClientOriginalExtension();
               echo $file_ex;
            $img_name=md5(uniqid()).".".$file_ex;
           echo $img_name;

           $path=public_path("/photos/projects");

            $img_place=$img[$i]->move($path,$img_name);
         // $lastid=$res->id;
         // echo $lastid;
         
           Image::create([
            'proj_id' =>$req->id,
            'image'=>$img_name,
          ]);

         }
        }
       
         $data->update
          ([
            'projName'    =>$req->projen,
            'proj_ar'     =>$req->projar,
            'cost'        =>$req->cost,
            'clientName'  =>$req->clientname,
            'description' =>$req->desc_en,
            'desc_ar'     =>$req->desc_ar,
            'emailClient' =>$req->email,

        ]);
        return redirect(app()->getLocale().'/admin/table');

        // $rule=$this->getrules();
        // $msg=$this->getmessage();
        // $vali=Validator::make($req->all(),$rule,$msg);
  
        //  if($vali->fails())
        //  {
        //     return redirect()->back()->withErrors($vali)->
        //     withInputs($req->all());

        //  }
         
        

    }
    function deleteData(Request $req){
       $id=$req->input('id');
    $image_ = DB::table('images')->where('proj_id', $id)->pluck('image');
    foreach ($image_ as $image_) {
           $path="/photos/projects/".$image_;
           unlink(public_path($path));


    }
        $image = DB::table('images')->where('proj_id', $id)->delete();

         DB::table('_designs')->where('id',$id)->delete();
     

    }
    // function getlogin(){
    //   return view('admin.auth.login');
    // }

    function premission()
     {
        // $p=Permission::create(['guard_name' => 'admin','name'=>'p']);
     //    $editproj=Permission::create(['guard_name' => 'admin','name'=>'editproj']);
     //    $addproj=Permission::create(['guard_name' => 'admin','name'=>'addproj']);
     //    $deladmin=Permission::create(['guard_name' => 'admin','name'=>'deladmin']);
     //    $addadmin=Permission::create(['guard_name' => 'admin','name'=>'addadmin']);
     //    $editadmin=Permission::create(['guard_name' => 'admin','name'=>'editadmin']);
 //         $role =Role::create(['guard_name' => 'admin','name'=>'admin']);
 //         $role->givePermissionTo('delproj','editproj','addproj','deladmin','addadmin','editadmin');
 // $rol4=Role::create(['guard_name' => 'admin','name'=>'emp']);
         // $rol3->givePermissionTo('delproj','addproj','editproj');
         // dd(\Auth::guard('admin'));
        // $user = \App\Models\User;
        // $user::assignRole($rol1);
        
        // Admin->givePermissionTo('delproj');
         
         //  $user = \App\Models\Admin::find(12);
         // return $user->assignRole('employee');
         // $user->assignRole('admin');
        // $user->removeRole('empl');
// return \App\Models\Admin::role('admin')->get();
    // return $user->removeRole('admin');
       // $user = \App\Models\Admin::find(2); 
      //  $role=Role::findById(5); 
      //  $permission=Permission::findById(3); 
      // return $role->givePermissionTo($permission);

 
     }
     


         
}
