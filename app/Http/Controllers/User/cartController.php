<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Productss;
use DB;

class cartController extends Controller
{
    private $base_url;
    private $token;
    public function __construct(){
        $this->base_url=env("MYFATOORAHBASEURL");
        $this->token=env("MYFATOORAHTOKEN");
    }
		public function addCart(Request $req)
    
    {
        $product = Productss::find($req->id);

        if(!$product) {

            abort(404);

        }

        $cart = session()->get('cart');

        // if cart is empty then this the first product
        if(!$cart) {

            $cart = [
                    $req->id => [
                        "name" => $product->name_en,
                        "quantity" => 1,
                        "price" => $product->price,
                        "photo" => $product->img
                    ]
            ];

            session()->put('cart', $cart);

            }

        // if cart not empty then check if this product exist then increment quantity
        elseif(isset($cart[$req->id])) {

            $cart[$req->id]['quantity']++;

            session()->put('cart', $cart);

            return redirect()->back()->with('success', 'Product added to cart successfully!');

        }

        // if item not exist in cart then add to cart with quantity = 1
        $cart[$req->id] = [
            "name" => $product->name_en,
            "quantity" => 1,
            "price" => $product->price,
            "photo" => $product->img
        ];

        session()->put('cart', $cart);

        return redirect()->back()->with('success', 'Product added to cart successfully!');
    }

    public function update(Request $request)
    {
        if($request->id and $request->quantity)
        {
            $cart = session()->get('cart');

            $cart[$request->id]["quantity"] = $request->quantity;

            session()->put('cart', $cart);

            
        }
    }

    public function removeCart(Request $request)
    {

        if($request->id)
        {

            $cart = session()->get('cart');

            if(isset($cart[$request->id]))
             {

                unset($cart[$request->id]);

                session()->put('cart', $cart);
            }

            
        }
    }
    public function checkOut()
    {


        if(session()->get('user'))
        {
            return view('front.checkout');
           


        }
        return redirect()->route('User_login');
	
    }
    public function continu_check(Request $req){
            $token=$this->token;
            $basURL=$this->base_url;
        $id_user=session()->get('user')[0];
       $data_user= DB::table('users')->where('id',$id_user)->get();
       foreach ($data_user as $user) {
          $name=$user->name;
          $email=$user->email;
       }

       $ccexpire=(explode('/', $req->cc_expiration));
       $ccMon=$ccexpire[0];
       $ccYear=$ccexpire[1];



     // return $req->all();
// token value to be placed here;
        // $token ="mytokenvalue";
        // $basURL = "https://apitest.myfatoorah.com";

                  $cart=session()->get('cart');
                  $cart_prod=array();
                foreach ($cart as $prod) 
                {
                    $data_p=array("ItemName"=>$prod['name'],
                        "UnitPrice"=>$prod['price'],
                        "Quantity"=>$prod['quantity']);
                    array_push($cart_prod,$data_p);
                  } 
                  
                 // $con_d=json_encode($cart_prod);

              $dts=[
                "PaymentMethodId"=>$req->paymentMethod,
            "CustomerName"=> $name,
            "NotificationOption"=> "ALL",
            "MobileCountryCode"=> "965",
            "CustomerMobile"=> "12345678",
            "CustomerEmail"=> $email,
            "InvoiceValue"=> $req->total,
            "DisplayCurrencyIso"=> "kwd",
            "CallBackUrl"=> "https://yoursite.com/success",
            "ErrorUrl"=> "https://yoursite.com/error",
            "Language"=> "en",
            "CustomerReference"=> "noshipping-nosupplier",
            "CustomerAddress"=> [
                    "Block"=> "string",
                    "Street"=> "string",
                    "HouseBuildingNo"=> "string",
                    "Address"=> "address",
                    "AddressInstructions"=> "string"
                ],
               "InvoiceItems"=>$cart_prod,
                 "SourceInfo"=> "string"

              ];
              $dtss= json_encode($dts);
             // return $dtss;
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "$basURL/v2/ExecutePayment",
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS =>$dtss
            ,

            CURLOPT_HTTPHEADER => 
            array("Authorization: Bearer $token","Content-Type: application/json"),
        ));

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        if ($err) 
        {
           return [
               'payment_success'=>false,
               'status'=>faild,
               'error'=>$err
           ];

        } 
        else 
        {
                // echo "$response '<br />'";

        }

         $json  = json_decode((string)$response, true);

          $payment_url = $json["Data"]["PaymentURL"];
  $xinvoice=$json["Data"]["InvoiceId"] ;
           # after getting the payment url call it as a post API        and pass card info to it
           # if you saved the card info before you can pass the        token for the api
    
           $curl = curl_init();
           curl_setopt_array($curl, array(
           CURLOPT_URL => "$payment_url",
           CURLOPT_CUSTOMREQUEST => "POST",
           CURLOPT_POSTFIELDS => 
           '{
            "Key":"'.$req->paymentMethod.'" ,
            "KeyType": "'.$xinvoice.'",
            "paymentType": "card",
           "card": 
           {"Number":
              "'.$req->cc_number.'",
              "expiryMonth":"'.$ccMon.'"
             ,
             "expiryYear":"'.$ccYear.'",
             "securityCode":"'.$req->cc_cvv.'"
             },
             "saveToken": false
         }',
           CURLOPT_HTTPHEADER => array("Authorization: Bearer $token","Content-Type: application/json"),
         ));



          
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($curl);
        $err = curl_error($curl);

         curl_close($curl);
         

 if ($err) {
           // echo "cURL Error #:" . $err;
      return [
            'payment_success'=>false,
            'status'=>'failed',
            'error'=>$err];
         } 
         $json=json_decode((string)$response,true);
         $PaymentId=$json["Data"]["PaymentId"];
         
         return "$response";
         //   return [
         //    'payment_success'=>true,
         //    'token'=>$PaymentId,
         //    'data'=>$json,
         //    'status'=>'successed'];
          
          }

          public function exc(){


          }

}