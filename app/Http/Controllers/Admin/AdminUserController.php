<?php

namespace App\Http\Controllers\Admin;
use App\Models\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
// use Spatie\Permission\Models\Role;
// use Spatie\Permission\Models\Permission;


class AdminUserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    
    
    public function adminInfo()
    {
    	$info=Admin::get();
    	return view('admin.adminpage',["info"=>$info]);

    }

    public function addAdmin(Request $req)
    {

    	$img=$req->file('img');

    	$file_ex=$img->getClientOriginalExtension();

    	$img_name=md5(uniqid()).".".$file_ex;

    	$path=public_path("/photos/admin");

    	$img_place=$img->move($path,$img_name);


    	Admin::create([
    		'username'=>$req->name,
    		'email'   =>$req->email,
    		'password'=>Hash::make($req->password),
    		'priv'    =>$req->priv,
    		'img'     =>$img_name


    	]);
    	return "done";




    }

    public function updateAdmin(Request $id){
        $edit=Admin::find($id->input('id'));
        return response()->json($edit);

    }
    public function updateA(Request $req){
         $edit=Admin::find($req->id);
         if($req->e_password)
         {

            $edit->update
            ([
                'password'=>Hash::make($req->e_password)

            ]);
            
         }
         if ($req->hasfile('e_img')) 
         {
            $img=$req->file('e_img');

            $file_ex=$img->getClientOriginalExtension();

            $img_name=md5(uniqid()).".".$file_ex;

            $path=public_path("/photos/admin");

            $img_place=$img->move($path,$img_name);
            $edit->update
            ([
                'img'=>$img_name
            ]);   
         }

         $edit->update
         ([
            'username'=>$req->e_name,
            'email'   =>$req->e_email,
            'priv'    =>$req->e_priv

         ]);
         return "done";
    }

    public function adminDel(Request $req)
    {
    	$del=Admin::find($req->input('id'));
    	$del->delete();
    	return 'done';



    }
}
