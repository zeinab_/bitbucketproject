<?php
namespace App\Http\Controllers\Admin\Auth;
use App\Models\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

// use Illuminate\Http\JsonResponse;

class LoginController extends Controller
{

     public function __construct()
    {
        $this->middleware('guest:admin')->except('logout');
    }
    
     

    public function showLoginForm(){
        return view('admin.auth.adminLogin');
    }
    public function login(Request $request){

        if (Auth::guard('admin')->attempt(['email'=>$request->email,
            'password'=>$request->password])) 
        {
          $session= DB::table('admins')->where('email',$request->email)->get();
          foreach ($session as $se) {
         $x=$request->session()->put('id',$se->id);

          }
  // $request->session()->get('id');
          

            
          
          // echo $session['id'];
            // $request->session()->put();
            return redirect()->intended(route('admin.design',app()->getLocale()));

        }
        return redirect()->intended(route('admin.login',app()->getLocale()));
    }
    protected function guard()
    {
        return Auth::guard('admin');
    }
    public function logout(Request $request)
    {
        Auth::guard('admin')->logout();
        return redirect()->intended(route('admin.login',app()->getLocale()));


    }
}
