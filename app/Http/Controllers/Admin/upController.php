<?php

namespace App\Http\Controllers\Admin;
use App\Repository\userRepository;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;

class upController extends Controller
{

    protected $model;
  
   
    public function __construct(User $user)
    {
       $this->model = new userRepository($user);
    }
    public function create(Request $request)
    {
    
       return $this->model->create($request->all());
    }
   public function userpage()

     {

       return view('admin.userPage',['data'=>$this->model->all()]);

     }
     public function edit(Request $id)
     {
     	
         return response()->json($this->model->find($id->input('id')));
     }
     public function updat(Request $re,$id){
     	// return print_r($re->all());
     	
         return $this->model->update($re->all(),$re->id);
     }
     public function del(Request $req){

     	 return $this->model->delete($req->id);
     }




     
 }
